#!/bin/bash

if [ -z "$1" ]
then
	1='.'
fi


for entry in $(find "$1" -maxdepth 10 -type f | grep "\.c")
do
	touch buffer
	sed -e "s///g" "$entry" > buffer
	cat buffer > "$entry"
	rm -rf buffer
done

for entry in $(find "$1" -maxdepth 10 -type f | grep "\.h")
do
	touch buffer
	sed -e "s///g" "$entry" > buffer
	cat buffer > "$entry"
	rm -rf buffer
done
