# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/06/24 23:57:01 by gabettin          #+#    #+#              #
#    Updated: 2019/09/07 05:32:40 by gabettin         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: clean fclean re dir all extra_dir extra_dir int_dir lst_dir mem_dir put_dir rand_dir str_dir gnl_dir




#==========files_name==========#
S_EXTRA		=	sources/extra/ft_doubleiter.c \
				sources/extra/ft_isalnum.c \
				sources/extra/ft_isalpha.c \
				sources/extra/ft_isascii.c \
				sources/extra/ft_isdigit.c \
				sources/extra/ft_isprint.c \
				sources/extra/ft_yo.c
S_INT		=	sources/int/ft_abs.c \
				sources/int/ft_atoi.c \
				sources/int/ft_atoil.c \
				sources/int/ft_digitnbr.c \
				sources/int/ft_itoa.c \
				sources/int/ft_itoal.c \
				sources/int/ft_pow.c \
				sources/int/ft_nbrbase.c \
				sources/int/ft_sqrt.c
S_LST 		=	sources/lst/ft_lstadd.c \
				sources/lst/ft_lstdel.c \
				sources/lst/ft_lstdelone.c \
				sources/lst/ft_lstiter.c \
				sources/lst/ft_lstlen.c \
				sources/lst/ft_lstmap.c \
				sources/lst/ft_lstnew.c \
				sources/lst/ft_lsttotab.c
S_MEM		=	sources/mem/ft_bzero.c \
				sources/mem/ft_memalloc.c \
				sources/mem/ft_memccpy.c \
				sources/mem/ft_memchr.c \
				sources/mem/ft_memcmp.c \
				sources/mem/ft_memcpy.c \
				sources/mem/ft_memdel.c \
				sources/mem/ft_memmove.c \
				sources/mem/ft_memset.c \
				sources/mem/ft_reallocforward.c \
				sources/mem/ft_safefree.c
S_PUT		=	sources/put/ft_print_lst_memory.c \
				sources/put/ft_print_memory.c \
				sources/put/ft_printaddr.c \
				sources/put/ft_putchar_fd.c \
				sources/put/ft_putchar.c \
				sources/put/ft_putendl_fd.c \
				sources/put/ft_putendl.c \
				sources/put/ft_putmorestr.c \
				sources/put/ft_putnbr_fd.c \
				sources/put/ft_putnbr.c \
				sources/put/ft_putnstr.c \
				sources/put/ft_putstr_fd.c \
				sources/put/ft_putstr.c \
				sources/put/ft_putnbrbase.c
S_RAND		=	sources/rand/ft_sysrandchar.c \
				sources/rand/ft_sysrandint.c \
				sources/rand/ft_sysrandlong.c \
				sources/rand/ft_truerandchar.c \
				sources/rand/ft_truerandint.c \
				sources/rand/ft_truerandlong.c
S_STR		=	sources/str/ft_strcat.c \
				sources/str/ft_strchr.c \
				sources/str/ft_strclen.c \
				sources/str/ft_strclr.c \
				sources/str/ft_strcmp.c \
				sources/str/ft_strcpy.c \
				sources/str/ft_strdel.c \
				sources/str/ft_strdup.c \
				sources/str/ft_strequ.c \
				sources/str/ft_striter.c \
				sources/str/ft_striteri.c \
				sources/str/ft_strjoin.c \
				sources/str/ft_strlcat.c \
				sources/str/ft_strlen.c \
				sources/str/ft_strmap.c \
				sources/str/ft_strmapi.c \
				sources/str/ft_strncat.c \
				sources/str/ft_strncmp.c \
				sources/str/ft_strndup.c \
				sources/str/ft_strncpy.c \
				sources/str/ft_strnequ.c \
				sources/str/ft_strnew.c \
				sources/str/ft_strnlen.c \
				sources/str/ft_strnstr.c \
				sources/str/ft_strrchr.c \
				sources/str/ft_strsplit.c \
				sources/str/ft_strstr.c \
				sources/str/ft_strsub.c \
				sources/str/ft_strtrim.c \
				sources/str/ft_tolower.c \
				sources/str/ft_toupper.c \
				sources/str/ft_charisin.c

#==========crusial_vars==========#
#default build name
BUILD		+=	Release
BUILD		:=	$(word 1, $(BUILD))
NAME		=	libft.a
DIR_SRC		=	sources
DIR_BLD		=	builds
DIR_INC		=	includes
DIR_OBJ		=	objects

#==========out_file============#
O_EXTRA		=	$(addprefix extra/, $(notdir $(S_EXTRA:.c=.o)))
O_INT		=	$(addprefix int/, $(notdir $(S_INT:.c=.o)))
O_LST		=	$(addprefix lst/, $(notdir $(S_LST:.c=.o)))
O_MEM		=	$(addprefix mem/, $(notdir $(S_MEM:.c=.o)))
O_PUT		=	$(addprefix put/, $(notdir $(S_PUT:.c=.o)))
O_RAND		=	$(addprefix rand/, $(notdir $(S_RAND:.c=.o)))
O_STR		=	$(addprefix str/, $(notdir $(S_STR:.c=.o)))
O_GNL_OUT	=	get_next_line/get_next_line.o

#==========to_compile============#
OBJECTS		=	$(addprefix $(DIR_OBJ)/$(BUILD)/, $(O_EXTRA))
OBJECTS		+= 	$(addprefix $(DIR_OBJ)/$(BUILD)/, $(O_INT))
OBJECTS		+=	$(addprefix $(DIR_OBJ)/$(BUILD)/, $(O_LST))
OBJECTS		+=	$(addprefix $(DIR_OBJ)/$(BUILD)/, $(O_MEM))
OBJECTS		+=	$(addprefix $(DIR_OBJ)/$(BUILD)/, $(O_PUT))
OBJECTS		+=	$(addprefix $(DIR_OBJ)/$(BUILD)/, $(O_RAND))
OBJECTS		+=	$(addprefix $(DIR_OBJ)/$(BUILD)/, $(O_STR))
OBJECTS		+=	$(DIR_OBJ)/$(BUILD)/$(O_GNL_OUT)

#==========default_compilation============#
CC			+=	clang
CC			:=	$(word 1, $(CC))
FLAGS		+=	-Wall -Wextra -Werror #-Weverything
#DFLAGS		=	-g3 -fPIC	it's no use !

#==========cosmetic============#
BUILD_EXTRA	=
BUILD_INT	=
BUILD_LST	=
BUILD_MEM	=
BUILD_PUT	=
BUILD_RAND	=
BUILD_STR	=
BUILD_GNL	=

#==========basic_rule============#
all: $(NAME)

$(NAME): dir
	@$(MAKE) main_rule

main_rule: $(OBJECTS)
	@ar rcs $(DIR_BLD)/$(BUILD)/$(NAME) $(OBJECTS)
	@cp $(DIR_BLD)/$(BUILD)/$(NAME) $(NAME)
	@echo "#==========Compilation result==========#"
	@echo "build name     : "$(BUILD)
	@echo "extra compiled : "$(words $(BUILD_EXTRA))
	@echo "int   compiled : "$(words $(BUILD_INT))
	@echo "lst   compiled : "$(words $(BUILD_LST))
	@echo "mem   compiled : "$(words $(BUILD_MEM))
	@echo "put   compiled : "$(words $(BUILD_PUT))
	@echo "rand  compiled : "$(words $(BUILD_RAND))
	@echo "str   compiled : "$(words $(BUILD_STR))
	@echo "gnl   compiled : "$(words $(BUILD_GNL))
	@echo "#======================================#"

local_lib:
	@cp $(DIR_BLD)/$(BUILD)/$(NAME) $(NAME)

re: fclean
	@$(MAKE) all

fclean: clean
	@rm -rf $(DIR_BLD)/$(BUILD)/$(NAME)
	@rm -rf $(NAME)

clean:
	@rm -rf $(DIR_OBJ)/$(BUILD)/*.o
	@rm -rf $(DIR_OBJ)/$(BUILD)/*/*.o

#==========compilation_rules============#
$(DIR_OBJ)/$(BUILD)/extra/%.o: sources/extra/%.c $(DIR_INC)/libft.h Makefile
	@$(CC) $(ARGS) $(FLAGS) -c -I$(DIR_INC) $< -o $@
	@$(eval BUILD_EXTRA += extra)
$(DIR_OBJ)/$(BUILD)/int/%.o: sources/int/%.c $(DIR_INC)/libft.h Makefile
	@$(CC) $(ARGS) $(FLAGS) -c -I$(DIR_INC) $< -o $@
	@$(eval BUILD_INT += int)
$(DIR_OBJ)/$(BUILD)/lst/%.o: sources/lst/%.c $(DIR_INC)/libft.h Makefile
	@$(CC) $(ARGS) $(FLAGS) -c -I$(DIR_INC) $< -o $@
	@$(eval BUILD_LST += lst)
$(DIR_OBJ)/$(BUILD)/mem/%.o: sources/mem/%.c $(DIR_INC)/libft.h Makefile
	@$(CC) $(ARGS) $(FLAGS) -c -I$(DIR_INC) $< -o $@
	@$(eval BUILD_MEM += mem)
$(DIR_OBJ)/$(BUILD)/put/%.o: sources/put/%.c $(DIR_INC)/libft.h Makefile
	@$(CC) $(ARGS) $(FLAGS) -c -I$(DIR_INC) $< -o $@
	@$(eval BUILD_PUT += put)
$(DIR_OBJ)/$(BUILD)/rand/%.o: sources/rand/%.c $(DIR_INC)/libft.h Makefile
	@$(CC) $(ARGS) $(FLAGS) -c -I$(DIR_INC) $< -o $@
	@$(eval BUILD_RAND += rand)
$(DIR_OBJ)/$(BUILD)/str/%.o: sources/str/%.c $(DIR_INC)/libft.h Makefile
	@$(CC) $(ARGS) $(FLAGS) -c -I$(DIR_INC) $< -o $@
	@$(eval BUILD_STR += str)
$(DIR_OBJ)/$(BUILD)/get_next_line/%.o: sources/get_next_line/get_next_line.c $(DIR_INC)/libft.h $(DIR_INC)/get_next_line.h Makefile
	@$(CC) $(ARGS) $(FLAGS) -c -I$(DIR_INC) $< -o $@
	@$(eval BUILD_GNL += gnl)

#==========dir_rules============#
dir: $(DIR_BLD) $(DIR_BLD)/$(BUILD) extra_dir int_dir lst_dir mem_dir put_dir rand_dir str_dir gnl_dir

extra_dir: $(DIR_OBJ) $(DIR_OBJ)/$(BUILD) $(DIR_OBJ)/$(BUILD)/extra
int_dir: $(DIR_OBJ) $(DIR_OBJ)/$(BUILD) $(DIR_OBJ)/$(BUILD)/int
lst_dir: $(DIR_OBJ) $(DIR_OBJ)/$(BUILD) $(DIR_OBJ)/$(BUILD)/lst
mem_dir: $(DIR_OBJ) $(DIR_OBJ)/$(BUILD) $(DIR_OBJ)/$(BUILD)/mem
put_dir: $(DIR_OBJ) $(DIR_OBJ)/$(BUILD) $(DIR_OBJ)/$(BUILD)/put
rand_dir: $(DIR_OBJ) $(DIR_OBJ)/$(BUILD) $(DIR_OBJ)/$(BUILD)/rand
str_dir: $(DIR_OBJ) $(DIR_OBJ)/$(BUILD) $(DIR_OBJ)/$(BUILD)/str
gnl_dir: $(DIR_OBJ) $(DIR_OBJ)/$(BUILD) $(DIR_OBJ)/$(BUILD)/get_next_line

$(DIR_BLD):
	@mkdir -p $(DIR_BLD)
$(DIR_BLD)/$(BUILD):
	@mkdir -p $(DIR_BLD)/$(BUILD)
$(DIR_OBJ):
	@mkdir -p $(DIR_OBJ)
$(DIR_OBJ)/$(BUILD):
	@mkdir -p $(DIR_OBJ)/$(BUILD)
$(DIR_OBJ)/$(BUILD)/extra:
	@mkdir -p $(DIR_OBJ)/$(BUILD)/extra
$(DIR_OBJ)/$(BUILD)/int:
	@mkdir -p $(DIR_OBJ)/$(BUILD)/int
$(DIR_OBJ)/$(BUILD)/lst:
	@mkdir -p $(DIR_OBJ)/$(BUILD)/lst
$(DIR_OBJ)/$(BUILD)/mem:
	@mkdir -p $(DIR_OBJ)/$(BUILD)/mem
$(DIR_OBJ)/$(BUILD)/put:
	@mkdir -p $(DIR_OBJ)/$(BUILD)/put
$(DIR_OBJ)/$(BUILD)/rand:
	@mkdir -p $(DIR_OBJ)/$(BUILD)/rand
$(DIR_OBJ)/$(BUILD)/str:
	@mkdir -p $(DIR_OBJ)/$(BUILD)/str
$(DIR_OBJ)/$(BUILD)/get_next_line:
	@mkdir -p $(DIR_OBJ)/$(BUILD)/get_next_line
