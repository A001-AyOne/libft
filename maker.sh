

if [ "$#" -lt 1 ]; then
	make
elif [ "$#" -lt 2 ]; then
	make $1
elif [ $2 = "Debug" ]; then
	make $1 BUILD=$2 ARGS="-g3 -fPIC"
elif [ $2 = "Release" ]; then
	make $1
elif [ "$#" -lt 3 ]; then
	make $1 BUILD=$2
else
	make $1 BUILD=$2 ARGS=$3
fi
