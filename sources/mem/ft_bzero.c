/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 03:19:38 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:38:58 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_bzero(void *s, ssize_t n)
{
	unsigned long	l;
	unsigned char	c;
	unsigned long	*ls;
	unsigned char	*cs;
	char			mod;

	mod = n % 8;
	l = 0x0000000000000000;
	if (mod != 0)
		ls = (unsigned long*)(s - ft_abs(n - ((n - n % 8) + 8)));
	else
		ls = (unsigned long*)s;
	c = 0x0000;
	cs = (unsigned char*)s;
	while (n > 8)
	{
		if (mod != 0)
			ls[n / 8] = l;
		else
			ls[n / 8 - 1] = l;
		n -= 8;
	}
	while (n > 0)
		cs[n-- - 1] = c;
}
