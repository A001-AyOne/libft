/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 05:33:05 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:39:08 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	if (n > 0 && ((unsigned char*)s)[0] != (unsigned char)c)
		return (ft_memchr(s + 1, c, n - 1));
	else if (n == 0)
		return (NULL);
	else if (((unsigned char*)s)[0] == (unsigned char)c)
		return ((void*)s);
	return (NULL);
}
