/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reallocforward.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/09 02:43:02 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:39:25 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_reallocforward(void **array, size_t len, void *d, size_t len2)
{
	void	*result;
	int		i;
	int		j;

	if ((result = malloc(len + len2)) == NULL)
		return ;
	i = 0;
	while (i < (int)len)
	{
		((char*)result)[i] = ((char*)array[0])[i];
		i++;
	}
	j = 0;
	while (j < (int)len2)
	{
		((char*)result)[i + j] = ((char*)d)[j];
		j++;
	}
	ft_safefree(array);
	*array = result;
}
