/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 02:33:30 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:39:23 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	if (len + 1 > 1)
		ft_memset(b + 1, c, len - 1);
	else
		return (b);
	((unsigned char*)b)[0] = (unsigned char)c;
	return (b);
}
