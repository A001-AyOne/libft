/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 04:56:57 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:39:19 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	unsigned char *buf_dst;
	unsigned char *buf_src;

	buf_src = (unsigned char *)src;
	buf_dst = (unsigned char *)dst;
	while (len--)
	{
		if ((unsigned long int)buf_src < (unsigned long int)buf_dst)
			buf_dst[len] = buf_src[len];
		else
			*(buf_dst++) = *(buf_src++);
	}
	return (dst);
}
