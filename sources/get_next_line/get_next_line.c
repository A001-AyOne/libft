/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/28 16:25:02 by gabettin          #+#    #+#             */
/*   Updated: 2019/08/30 02:59:04 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int	ft_check(char d[MAX_FD][BUFF_SIZE], const int fd, char **l, int s)
{
	size_t	rnd;
	int		i;

	if (ft_strlen((char*)d[fd]) != (rnd = ft_strclen((char*)d[fd], '\n')))
	{
		if (s == 0)
		{
			ft_safefree((void**)l);
			*l = ft_strndup((char*)d[fd], rnd);
		}
		else
			ft_reallocforward((void**)l, s, d[fd], rnd);
		ft_reallocforward((void**)l, s + rnd, "\0", 1);
		i = -1;
		while (++i < BUFF_SIZE)
			d[fd][i] = (i + rnd + 1 >= BUFF_SIZE) ? '\0' : d[fd][i + rnd + 1];
		return (1);
	}
	return (0);
}

static int	excet_crlf(char d[MAX_FD][BUFF_SIZE], const int fd, char **l, int s)
{
	int i;
	int rnd;

	rnd = ft_strclen((char*)d[fd], '\n');
	ft_reallocforward((void**)l, s + rnd, "\0", 1);
	i = -1;
	while (++i < BUFF_SIZE)
		d[fd][i] = (i + rnd + 1 >= BUFF_SIZE) ? '\0' : d[fd][i + rnd + 1];
	return (1);
}

int			get_next_line(const int fd, char **line)
{
	static char	d[MAX_FD][BUFF_SIZE];
	ssize_t		ret;
	size_t		size;

	if (fd >= MAX_FD || fd < 0 || line == NULL)
		return (-1);
	*line = NULL;
	if (ft_check(d, fd, line, 0) == 1)
		return (1);
	ft_reallocforward((void**)line, 0, d[fd], size = ft_strlen((char*)d[fd]));
	ft_reallocforward((void**)line, size, "\0", 1);
	ft_bzero(d[fd], BUFF_SIZE);
	while ((ret = read(fd, d[fd], BUFF_SIZE)) > 0)
	{
		if (ft_check(d, fd, line, size) == 1)
			return (1);
		ft_reallocforward((void**)line, size, d[fd], ret);
		ft_reallocforward((void**)line, size + ret, "\0", 1);
		if (line == NULL)
			return (-1);
		((size += ret) != (size_t)0) ? ft_bzero(d[fd], BUFF_SIZE) : 0;
	}
	if ((int)ret == -1)
		return (-1);
	return ((*line[0] == '\0') ? 0 : excet_crlf(d, fd, line, size));
}
