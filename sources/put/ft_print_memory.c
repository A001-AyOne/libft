/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_memory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/06 03:47:27 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:39:37 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_print_hexa_mem(void *addr, size_t mem_len, size_t line_len)
{
	unsigned char	*a;
	int				space;

	a = (unsigned char *)addr;
	space = 0;
	while (mem_len-- > 0 && (unsigned long)space < line_len)
	{
		ft_putchar((*a / 16 >= 10) ? ((*a / 16) - 10) + 'a' : *a / 16 + '0');
		ft_putchar((*a % 16 >= 10) ? ((*a % 16) - 10) + 'a' : *a % 16 + '0');
		a++;
		if (space++ % 2)
			ft_putchar(' ');
	}
	while ((unsigned long)space < line_len)
	{
		if (space % 2)
			ft_putchar(' ');
		ft_putstr("  ");
		space++;
	}
	if (space % 2 == 0 || mem_len > 0)
		ft_putchar(' ');
}

static void	ft_print_mem(void *addr, size_t mem_len, size_t line_len)
{
	char	*a;
	int		index;

	a = (char *)addr;
	index = 0;
	while (mem_len-- > 0 && (unsigned long)index++ < line_len)
	{
		if (*a <= ' ')
			ft_putchar('.');
		else
			ft_putchar(*a);
		a += 1;
	}
	while ((unsigned long)index++ < line_len)
		ft_putchar(' ');
}

static void	ft_print_null(void *addr, size_t line_len)
{
	ft_printaddr(addr);
	ft_putstr(" : ");
	ft_print_hexa_mem(addr, 0, line_len);
	ft_putstr(": ");
	ft_print_mem(addr, 0, line_len);
	ft_putstr(" |\n");
}

void		ft_print_memory(void *addr, size_t mem_len, size_t line_len)
{
	unsigned long int	addrr;

	addrr = (unsigned long int)addr;
	if (mem_len == 0 || addr == NULL)
	{
		return (ft_print_null(addr, line_len));
	}
	while (mem_len > 0)
	{
		ft_printaddr((void *)addrr);
		ft_putstr(" : ");
		ft_print_hexa_mem((void *)addrr, mem_len, line_len);
		ft_putstr(": ");
		ft_print_mem((void *)addrr, mem_len, line_len);
		ft_putstr(" |\n");
		mem_len -= (mem_len > line_len) ? line_len : mem_len;
		addrr += line_len;
	}
}
