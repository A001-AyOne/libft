/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 22:15:00 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:41:27 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strnequ(char const *s1, char const *s2, size_t n)
{
	unsigned int i;

	i = 0;
	if (n == 0 || s1 == NULL || s2 == NULL)
		return (1);
	while (s1[i] == s2[i] && s1[i] != '\0' && i + 1 < n)
		i++;
	return ((s1[i] == s2[i]) ? 1 : 0);
}
