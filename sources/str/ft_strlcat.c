/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/01 20:06:21 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:41:01 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dest, const char *src, size_t size)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	while (dest[i] != '\0' && i < size)
		i++;
	j = 0;
	while (src[j] != '\0')
		j++;
	if (dest[i] != '\0')
		return (i + j);
	while (size-- > 0 && *dest != '\0')
		dest++;
	size++;
	while (size-- > 1 && *src != '\0')
		*(dest++) = *(src++);
	*dest = '\0';
	return (i + j);
}
