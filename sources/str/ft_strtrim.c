/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 00:34:24 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:41:53 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	char			*result;
	unsigned int	i;

	if (s == NULL)
		return (NULL);
	while (*s == ' ' || *s == '\t' || *s == '\n')
		s++;
	i = (unsigned int)ft_strlen(s);
	while (i > 0 && (s[i - 1] == ' ' || s[i - 1] == '\t'
		|| s[i - 1] == '\n'))
		i--;
	if (i == 0 && (result = malloc(sizeof(char))) == NULL)
		return (NULL);
	else if ((result = malloc(sizeof(char) * (i + 1))) == NULL)
		return (NULL);
	ft_strncpy(result, s, i);
	result[i] = '\0';
	return (result);
}
