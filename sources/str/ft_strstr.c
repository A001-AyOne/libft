/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/30 15:00:04 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:41:45 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	const char *old_find;
	const char *old_str;

	if (*needle == '\0')
		return ((char*)haystack);
	old_find = needle;
	old_str = haystack;
	while (*haystack != '\0')
	{
		needle = old_find;
		while (*haystack++ == *(needle++))
			if (*needle == '\0')
				return ((char*)(haystack + (old_str - haystack)));
		old_str++;
		haystack = old_str;
	}
	return (NULL);
}
