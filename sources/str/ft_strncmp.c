/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/31 12:36:11 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:41:21 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	if (n == 0)
		return (0);
	return (((unsigned char)*s1 == (unsigned char)*s2 && *s1 != '\0'
		&& n > 1) ? ft_strncmp(s1 + 1, s2 + 1, n - 1)
			: (unsigned char)*s1 - (unsigned char)*s2);
}
