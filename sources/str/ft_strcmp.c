/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/31 12:27:17 by gabettin          #+#    #+#             */
/*   Updated: 2019/01/23 16:15:18 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strcmp(const char *s1, const char *s2)
{
	return (((unsigned char)*s1 == (unsigned char)*s2 && *s1 != '\0'
		&& *s2 != '\0') ? ft_strcmp(s1 + 1, s2 + 1)
			: (unsigned char)*s1 - (unsigned char)*s2);
}
