/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_randint.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 05:58:31 by gabettin          #+#    #+#             */
/*   Updated: 2019/04/17 15:45:24 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_sysrandint(int min, int max)
{
	unsigned char	result[4];
	int				file;
	int				res;

	if (max < min)
		return (0);
	if (max == min)
		return (max);
	file = open("/dev/random", O_RDONLY);
	read(file, result, 4);
	close(file);
	res = ((int*)result)[0];
	return ((res % (max - min) + min));
}
