/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sysrandlong.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 06:38:53 by gabettin          #+#    #+#             */
/*   Updated: 2019/04/17 15:46:20 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

long	ft_sysrandlong(long min, long max)
{
	unsigned char	result[8];
	int				file;
	long			res;

	if (max < min)
		return (0);
	if (max == min)
		return (max);
	file = open("/dev/random", O_RDONLY);
	read(file, result, 8);
	close(file);
	res = ((long*)result)[0];
	return (res % (max - min) + min);
}
