/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_randchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 06:32:22 by gabettin          #+#    #+#             */
/*   Updated: 2019/04/17 15:45:21 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	ft_sysrandchar(char min, char max)
{
	unsigned char	result[1];
	int				file;
	char			res;

	if (max < min)
		return (0);
	if (max == min)
		return (max);
	file = open("/dev/random", O_RDONLY);
	read(file, result, 1);
	close(file);
	res = ((char*)result)[0];
	return (res % (max - min) + min);
}
