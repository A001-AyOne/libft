/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 07:11:19 by gabettin          #+#    #+#             */
/*   Updated: 2019/09/07 01:39:19 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

long		ft_sqrt(long nbr)
{
	long i;

	i = 1;
	while (i * i < nbr)
		i++;
	if (i == nbr)
		return (i);
	return (0);
}

long		ft_prev_sqrt(long nbr)
{
	long i;

	i = 1;
	while (i * i < nbr)
		i++;
	return (i);
}
