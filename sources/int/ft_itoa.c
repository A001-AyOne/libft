/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/09 02:32:27 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:38:22 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	char			*result;
	size_t			len;
	unsigned int	b;

	len = (n >= 0) ? ft_digitnbr(n) : ft_digitnbr(n) + 1;
	if ((result = (char*)malloc(sizeof(char) * (len + 1))) == NULL)
		return (NULL);
	ft_memset(result, 0, len + 1);
	if (n < 0)
	{
		result[0] = '-';
		b = (unsigned int)-n;
	}
	else
		b = (unsigned int)n;
	while (len-- != ((n >= 0) ? 0 : 1))
	{
		result[len] = (b % 10) + '0';
		b /= 10;
	}
	return (result);
}
